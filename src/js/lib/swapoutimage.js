// import { TimelineMax } from "gsap/src/minified/TimelineMax.min.js";
import $ from 'jquery'

class SwapOutImage {

    constructor (image) {
        this.image = document.querySelector(image)
        this.filename = null;
        
     }

     init(response){
         console.log('swap out image', );
         this.filename = response.data.filename;
         this.fadeOut(this.filename);
     }

     fadeOut(filename){
         $(this.image).fadeOut('ease-out', ()=>{
            this.image.src = `./ss/${filename}.png`;
            this.fadeIn();
         });
     }
     fadeIn(){
         $(this.image).fadeIn('ease-in');
     }
}

export default (image)=>{

   return  new SwapOutImage(image);

}