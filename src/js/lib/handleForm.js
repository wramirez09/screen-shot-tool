
import Fetch from '../lib/fetch.js';
import Toast from 'bootstrap/js/dist/toast';
import $ from 'jquery'
import loader from './loader';


class FormHandler {

    constructor(form, submit, crawlerCheckbox){
        this.form = document.querySelector(form);
        this.inputs = document.querySelectorAll(`${form} > .url-input`);
        this.values = {};
        this.submit =  document.querySelector(submit);
        this.userAgentString = navigator.userAgent;
        this.screenWidth = window.screen.width;
        this.screenHeight = window.screen.height;
        this.multipleshot = false;
        this.mobileshot = false;
        this.crawler = false;
        this.singleurl = null;
        this.crawlerCheckbox = document.querySelector(crawlerCheckbox);
        this.loader = new loader();
    }

    init() {
        
        this.submit.addEventListener('click', (event)=>{
            this.handleSubmit(event);
        })

        this.crawlerCheckbox.addEventListener('change', (event)=>{
            this.handleCrawlerCheckBox(event);
        })
    }

    getUrlFromInputs() {
       
       let urls;
        this.inputs.forEach((input)=>{
            urls = input.value
            
        })

        return urls;
    }

    setTheObject() {

        this.values['userAgent'] = this.userAgentString;
        this.values["screenWidth"] = this.screenWidth;
        this.values["screenHeight"] = this.screenHeight;
        this.values["multipleshot"] = this.multipleshot;
        this.values["mobileshot"] = this.mobileshot;
        this.values["crawler"] = this.crawler;
        this.values["filename"] = '';
        this.values["singleurl"] = this.getUrlFromInputs();

        return this.values;
    }

    handleSubmit(event){
        event.preventDefault();
        let obj = this.setTheObject();
        let getSS = Fetch(obj);
        getSS.fetchy();
        this.showToast();
    }

    showToast(){
    
        this.loader.show();
        
    }

    handleCrawlerCheckBox(event){

        const checkbox = event.target

        if(checkbox.checked == true){
            this.crawler = checkbox.checked
            this.setTheObject();
            console.log('crawler check box checked', this.values)
        }
        else {
            this.crawler = checkbox.checked;
            this.setTheObject();
            console.log('crawler check box not checked', this.values)
        }
        
    }

}

export default (form, submit, crawlerCheckbox)=>{
    
    return new FormHandler(form, submit, crawlerCheckbox);
};