var Url = require('url-parse');
import Loader from './loader';

class CreateCards {
    constructor(imagesArray){
        this.imagesArray = imagesArray;
        this.loader = new Loader();
    }

    init (){
        this.createcard(this.imagesArray);
    }

    extractHostname(url) {

        var hostname;

        if (url.indexOf("://") > -1) {
            var arrayurl = url.split('/')
            hostname = arrayurl[0] + "//" + arrayurl[2]
                hostname = url.split('/')[2];
        } else {
            hostname = url.split('/')[0];
        }

        //find & remove "?"
        // hostname = hostname.split('?')[0];

        return hostname;
    }
    createcard(){
        const images = []
        this.imagesArray.forEach( (image, i) => {
            // var template = document.createElement('div');
            // template.classList.add('card', 'col-sm-4');
            // const ssv = document.querySelector('.row');           
            const imageObj = new Image(); 
            const url = new Url(image);
            const newImagePath = url.pathname;
            const newImagePathName = newImagePath.replace(/^\/|\/$/g, '') + '.png'
            imageObj.src = `./ss/${url.host}/${newImagePathName}`;
            images.push(imageObj);
            // template.appendChild(imageObj);
            // ssv.appendChild(template);
            
            
        });
        var template,ssv
        images.forEach((imageObj)=>{
            template = document.createElement('div');
            template.classList.add('card', 'col-sm-4');
            ssv = document.querySelector('.row');
            template.appendChild(imageObj);
            ssv.appendChild(template);
        })

        console.log(images, template, ssv);
        this.loader.hide()
    }
}


export default (imagesArray)=>{
    return new CreateCards(imagesArray);
}