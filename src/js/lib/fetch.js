import axios from 'axios';
import SwapOutImage from './swapoutimage';
import CreateCards from './createCards';
import loader from './loader'
class Fetch {
    constructor(dataObj){
        this.dataObj = dataObj;
        this.ss_endpoint = '/phantom-capture/';
        this.swapoutimage = new SwapOutImage('.image');
        this.response = null;
        this.loader = new loader();
    }

    fetchy(){
        console.log(this.ss_endpoint)
        axios.post(this.ss_endpoint, this.dataObj).then((response)=>{
            this.response = response
            if(response && this.response.data.crawler === true){
                // length of file directory is same as urls array then call create cards 
                // fs.readdir(`./ss/${response.data.filename}/`, (err, files) => {
                //     console.log(files.length);
                //   });
                CreateCards(this.response.data.multiurl).init();   
                console.log(response);
                
            }
            else {
                console.log('crawler false' ,this.response.crawler)
                this.swapoutimage.init(this.response);
                // hide loader 
                this.loader.hide();
                // swap out src of dl button
                const dl_btn = document.querySelector('.download-button');
                console.log(this.response.data)
                dl_btn.href = `./ss/${this.response.data.filename}.png`;

            }
            
        });
    }
}


export default (dataObj) => {
    return new Fetch(dataObj);
}