export default class loader {
    constructor(){
        this.loader = document.querySelector('.loader-wrapper');
        this.body = document.getElementsByTagName('body')[0];
        this.downloadButton = document.querySelector('.download-button');
    }


    show(){
        
        this.loader.style.opacity = 1;
        this.body.classList.add('no-scroll');
        // this.downloadButton.style.opacity = 1
    }

    hide() {
        this.loader.style.opacity = 0;
        this.body.classList.remove('no-scroll');
        this.downloadButton.style.opacity = 1
    }

}
