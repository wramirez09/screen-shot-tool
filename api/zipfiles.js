var shell = require("shelljs");

module.exports.zipit = function(path, req, res) {
    
    if (shell) {

        shell.exec(`zip -r public/ss/screenshots.zip ${path}`, function() {});
        req.body.multi = "true";
        req.body.zippath = "/screenshots.zip".toString();
    }
};

module.exports.removefiles = function() {
    // remove all old screenshot in directory  
    shell.exec('rm -rf ./public/ss/', function() {});
}

module.exports.removezip = function() {
    // remove all old screenshot in directory  
    shell.exec('rm -rf ./public/ss/ss.zip', function() {});
}