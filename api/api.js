var express = require('express')
var app = express();
var singleshot = require('./singleshot');
var multishot = require('./multishot');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


module.exports.postIndex = function(req, res) {
    console.log('postIndex');
    res.send({'key':'test'})
}

module.exports.phantomscreencapture = function(req, res) {
    

    if (req.body.multipleshot === false) {

        singleshot.singleshot(req, res);

    } else if (req.body.multipleshot === true) {

        multishot.multishot(req, res);

    }
}