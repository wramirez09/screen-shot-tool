// var webshot = require('webshot');
const puppeteer = require('puppeteer');
const zipfiles = require('./zipfiles');
const shell = require("shelljs");
const fs = require('fs');
process.setMaxListeners(0);

module.exports.multishot = (req, res, host) => {

    var dir = host.replace(/^https?\:\/\//i, "").replace(/\/$/, "");
    shell.rm( '-rf','./public/ss/**');
    shell.mkdir(`./public/ss/`);
    shell.mkdir(`./public/ss/${dir}`);

    req.body.multiurl.forEach((url, i) => {
        
        var path = url.replace(/^https?\:\/\//i, "").replace(/\/$/, "") + ".png";
        console.log('path', path);
        
            (async () => {
                console.log(`shooting ${path}`);    
                const browser = await puppeteer.launch();
                const page = await browser.newPage();
                await page.setViewport({width: parseInt(req.body.screenWidth, 10) , height: parseInt(req.body.screenHeight, 10)})
                await page.goto(url);
                await page.screenshot({ path: `./public/ss/${path}`, fullPage: true });
                await browser.close();    
                await zipfiles.zipit(`./public/ss/`, req, res);    
                await res.send(req.body);
                
                
            })();
    });
};
