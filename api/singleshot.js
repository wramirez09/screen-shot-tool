var express = require('express')
var app = express(); 
const puppeteer = require('puppeteer');
var zipfiles = require('./zipfiles');
var crawler = require('./crawler');
var options = require('./options');
module.exports.singleshot = function(req, res) {

    var urly = req.body.singleurl,
        userAgent = req.body.userAgent,
        filename,
        fileTypeExtension;
    

    if (req.body.filename !== "") {

        filename = req.body.filename

    } else {
        
        filename = urly.replace(/^https?\:\/\//i, "").replace(/\/$/, "");
       
    }

    if (req.body.fileTypeExtension) {

        fileTypeExtension = req.body.fileTypeExtension;
    } else {

        fileTypeExtension = "png"
    }

    // add back to req object 
    finalFileName = filename + "." + fileTypeExtension;
    req.body.fileTypeExtension = fileTypeExtension;
    req.body.filename = filename

    if (req.body.crawler == true) {
        // crawler
        crawler.crawl(req, res);

    } else {
        
        (async () => {

            await console.log(`shooting ${finalFileName}`);
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.setViewport({width: parseInt (req.body.screenWidth, 10), height: parseInt (req.body.screenHeight, 10)})
            await page.goto(urly);
            await page.screenshot({path: "./public/ss/" + finalFileName, fullPage: true });
            await res.status(200).send(req.body);
            await browser.close();
            
          })();
    }

}