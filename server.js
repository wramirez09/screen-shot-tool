var express = require('express');
var app = express();
var api = require('./api/api');
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.all("/phantom-capture/", api.phantomscreencapture.bind(this));

app.post("/", api.postIndex.bind(this));

var port = process.env.PORT || 3069;

app.listen(port, function(req, res) {
    console.log('app listening on port ' + port);

});